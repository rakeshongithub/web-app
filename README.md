# Testing Web App for testing #

Using Grunt + Bower with Laravel and Bootstrap

### What is Grunt ###

Official definition: Grunt: The JavaScript Task Runner

Grunt is basically a tool that automates your frontend tasks, but it goes beyond that if you want it. It can upload your changed files to CDN networks, copy files to production environment, optimize images and more. There are inumerous plugins for all these functions and if you can't find one that suits your needs, you can always write your own.

### Installation ###

Grunt runs on Node.js, so if you don't have npm installed already, go ahead and install it.

To install Grunt's command line interface run:

npm install -g grunt-cli
With the flag -g you installed it globally and now you can access it from anywhere on your system.

To initialize a new Grunt project from your project's directory run


```
#!javascript
npm init
```

and follow the instructions. The command will create your package.json file, necessary for any npm project.


```
#!javascript

{
  "name": "gruntRocks",
  "version": "0.0.1",
  "description": "Using Grunt with Laravel and Bootstrap",
  "main": "Gruntfile.js",
  "repository": {
    "type": "git",
    "url": "https://github.com/elena-kolevska/grunt-laravel.git"
  },
  "keywords": [
    "Laravel",
    "Grunt",
    "Bootstrap",
    "elenakolevska.com"
  ],
  "author": "Elena Kolevska",
  "license": "BSD",
  "bugs": {
    "url": "https://github.com/elena-kolevska/grunt-laravel/issues"
  }
}
```

Finally, let's install grunt and some plugins as dependencies. We'll use:


```
#!javascript

"grunt-contrib-concat": For concatenation;
"grunt-contrib-less": For LESS compilation
"grunt-contrib-uglify": For javascript minification
"grunt-phpunit": For running PhpUnit tests
"grunt-contrib-watch": To watch files for changes
```


####In your terminal run:####


```
#!javascript

npm install grunt --save-dev 
npm install grunt-contrib-concat --save-dev 
npm install grunt-contrib-less --save-dev 
npm install grunt-contrib-uglify --save-dev 
npm install grunt-contrib-watch --save-dev 
npm install grunt-phpunit --save-dev
```


That will install the dependencies and because we defined the --save-dev flag it will add them to the package.json file.

 

- Bower

Installing bower couldn't be simpler. Just go:


```
#!javascript

npm install -g bower
```


And you have it.

 

- Frontend dependencies

For this tutorial we'll use Bootstrap and jQuery so let's install them with Bower:

You could use only bower install bootstrap and that would work, but if you want to save a list of dependencies go ahead and create a bower.json file containing only a project name:


```
#!javascript

{
  "name": "gruntRocks"
}
```

Now run:


```
#!javascript

bower install bootstrap -S

```

The -S flag will save the dependency in the bower.json file and later you can just run bower install to replicate the exact front-end dependencies of your project. If you're not one of those guys paranoid about github just dying one day and our poor selves left without our packages, you can freely add bower_components to your .gitignore file and just track bower.json.

I know you noticed already; I'm forgetting about jQuery. But actually we already got it. In its bower.json file Bootstrap defines that it depends on jQuery, so it got automatically pulled in together with Bootstrap.

The default installation directory is bower_components. If you wish, you can change that in a .bowerrc file in the root of the project, as well as many other configuration options, but that isn't really the key point of this tutorial, so let's keep our focus on grunt for now and if you'd like to play around with bower some more later, go ahead and check out their web site

Here's how our components folder turned out (only the parts that concern us):


```
#!javascript

/bower_components
    /bootstrap
      /dist
        /js
        - bootstrap.js
        - bootstrap.min.js
      /less
      - alerts.less
      - badges.less
      - bootstrap.less
      ...
      ...
      - variables.less
      - wells.less
    /jquery
      - jquery.js
      - jquery.min.js
```