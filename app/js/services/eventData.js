"use strict";

eventsApp.factory("eventData", function($resource, $q){

	var resource = $resource("data/:id.json", {id:"@id"});

	return {
			getEvent: function(){

				var deffered = $q.defer();

				resource.get({id:"event"},
						function (event) {
							deffered.resolve(event);
						},
						function (status) {
							deffered.reject(status);
						});

					return deffered.promise;
			},

			save : function (event) {

				var deffered = $q.defer();
				event.id = 999;
				resource.save(event,
					function (data, status, header, config) {deffered.resolve(data);},
					function (data, status, header, config) {deffered.reject(data);}
				);

				return deffered.promise;
			}
		}
});