"use strict";

eventsApp.factory("navData", function($resource, $q){
	return {
			getMenu: function(){
				var deferred = $q.defer();

				$resource("data/:navId.json", {id: "@navId"})
				.get({navId:"nav"},
					function (event) {
						deferred.resolve(event);
					},
					function (status) {
						deferred.reject(status);
					});

				return deferred.promise;
			}
		}
});