"use strict";

eventsApp.factory("templateData", function($resource, $q){
	return {
			getTempInfo : function () {
			var deferred = $q.defer();
			$resource("data/:tempId.json", {id:"@tempId"})
				.get({tempId:"temp"},
					function (event) {
						deferred.resolve(event);
					},
					function (response) {
						deferred.reject(response);
					});

				return deferred.promise;
			}
	}
});