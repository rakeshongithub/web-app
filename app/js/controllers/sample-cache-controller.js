"use strict";

eventsApp.controller('sampleCacheCtrl', function ($scope, templateData, navData, myCache) {
		$scope.event = {};
		//Template info services called
		$scope.headFooterInfo = templateData.getTempInfo()
		.then(
			function (event) {$scope.headFooterInfo = event; },
			function (response) {console.log(response);}
		);

		//Navigation
		$scope.menus = navData.getMenu()
		.then(
			function (event) {$scope.menus = event; },
			function (response) {console.log(response);}
		);


		$scope.addToCache = function (key, value) {
			myCache.put(key, value);
		};

		$scope.readFromCache = function (key) {
			return myCache.get(key);
		};

		$scope.getCacheStats = function () {
			var obj = myCache.info();
			return obj;
		};
});