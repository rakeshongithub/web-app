"use strict";

eventsApp.controller("editProfileCtrl", function ($scope, templateData, navData) {

		//Template info services called
		$scope.headFooterInfo = templateData.getTempInfo()
		.then(
			function (event) {$scope.headFooterInfo = event; },
			function (response) {console.log(response);}
		);

		//Navigation
		$scope.menus = navData.getMenu()
		.then(
			function (event) {$scope.menus = event; },
			function (response) {console.log(response);}
		);
		
});