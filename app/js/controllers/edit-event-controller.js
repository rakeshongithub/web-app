"use strict";

eventsApp.controller('editEventCtrl', function ($scope, templateData, navData, eventData) {
		$scope.event = {};
		//Template info services called
		$scope.headFooterInfo = templateData.getTempInfo()
		.then(
			function (event) {$scope.headFooterInfo = event; },
			function (response) {console.log(response);}
		);

		//Navigation
		$scope.menus = navData.getMenu()
		.then(
			function (event) {$scope.menus = event; },
			function (response) {console.log(response);}
		);

		// Save Event
		$scope.saveEvent = function(event, newEventForm){
			if(newEventForm.$valid){
				//alert("Event '" + event.ename + "' Saved Successfully !");
				eventData.save(event)
					.then(
						function (response) { console.log('Success ' + JSON.stringify(response)); },
						function (response) { console.log('Failure ' + JSON.stringify(response)); }
					)
			}
		};

		//Cancel Add New Event
		$scope.cancelEdit = function(){
			window.location = "/web-app/app/index.html"
		}
});