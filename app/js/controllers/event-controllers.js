"use strict";

eventsApp.controller('eventCtrl', function ($scope, eventData, templateData, navData, $anchorScroll){

		/*Sort order filter default value added*/
		$scope.sortorder = "name";

		//Template info services called
		$scope.headFooterInfo = templateData.getTempInfo()
		.then(
			function (event) {$scope.headFooterInfo = event; },
			function (response) {console.log(response);}
		);

		//Navigation
		$scope.menus = navData.getMenu()
		.then(
			function (event) {$scope.menus = event; },
			function (response) {console.log(response);}
		);

		//Event Data services called
		$scope.events = eventData.getEvent()
		.then(
			function (event) {$scope.events = event; },
			function (response) {console.log(response);}
		);

		//Up vote function
		$scope.upVoteSession = function(session){
			session.upVoteCount++;
		};

		//Down vote function
		$scope.downVoteSession = function(session){
			session.upVoteCount--;
		};

		$scope.scrollToSession = function () {
			$anchorScroll();
		}
});