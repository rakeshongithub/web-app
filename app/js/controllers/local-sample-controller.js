"use strict";

eventsApp.controller('localSampleCtrl', function ($scope, templateData, navData, $locale, $timeout) {
		$scope.event = {};
		//Template info services called
		$scope.headFooterInfo = templateData.getTempInfo()
		.then(
			function (event) {$scope.headFooterInfo = event; },
			function (response) {console.log(response);}
		);

		//Navigation
		$scope.menus = navData.getMenu()
		.then(
			function (event) {$scope.menus = event; },
			function (response) {console.log(response);}
		);


		//My Date and My date formate
		console.log($locale)
		$scope.myDate = Date.now();
		$scope.myFormat = $locale.DATETIME_FORMATS.fullDate;

		// $timeout example
		var promise = $timeout(function () {
			$scope.name = "Amitabh Bachachan"
		}, 3000);

		//Cancle timeout function
		$scope.cancel = function (argument) {
			console.log("Timeout Canceled")
			$timeout.cancel(promise)
		}

});