"use strict";

eventsApp.controller('sampleCompileCtrl', function ($scope, templateData, navData, $compile, $parse) {
		$scope.event = {};
		//Template info services called
		$scope.headFooterInfo = templateData.getTempInfo()
		.then(
			function (event) {$scope.headFooterInfo = event; },
			function (response) {console.log(response);}
		);

		//Navigation
		$scope.menus = navData.getMenu()
		.then(
			function (event) {$scope.menus = event; },
			function (response) {console.log(response);}
		);


		//Example 1: $parse
		var fn = $parse("1 + 2");
		console.log(fn());

		//Example 2: $parse with getter
		var getter = $parse("event.name");

		var context1 = {event:{name: "AngularJS Boot Camp"}};
		var context2 = {event:{name: "Code Camp"}};

		console.log(getter(context1));
		console.log(getter(context2));
		console.log(getter(context2, context1));

		//Example 3: $parse with setter
		var setter = getter.assign;
		setter(context1, "Code Reset");

		console.log(context1.event.name)

		//$markup example
		$scope.appendDivToMarkup = function (markup) {
			$compile(markup)($scope).appendTo(angular.element("#appendHere"));
		}

});